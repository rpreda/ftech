var valueChanged = function (input, value) {
    var self = this;

    var path = input.split(".");
    var key = path.shift();
    var state = self.state[key];

    function follow (path, state, val) {
        var key = path.shift();
        if (path.length === 0) {
            state[key] = val;
            return state;
        } else {
            state[key] = follow(path, state[key], val);
            return state;
        }
    }

    state = follow(path, state, value);

    var newState = {};
    newState[key] = state;

    self.setState(newState);
};

export default {
    valueChanged: valueChanged
};