import React from 'react';
import {Link} from 'react-router';
import CartActions from './actions/CartActions';
import CartStore from './stores/CartStore';

var RestRow = React.createClass({
	render() {
		var restaurants = [];
		this.props.elements.map((restaurant, index) => {
			restaurants.push(
					<div className="col-sm-6 col-md-3" key = {index}>
					    <div className="thumbnail">
					      <img src={restaurant.image} alt="restaurant thumbnail"/>
					      <div className="caption">
					        <h3>{restaurant.name}</h3>
					        <p>{restaurant.desc}</p>
					        <p>
					        	<Link to={'/menu/' + restaurant._id} className="btn btn-primary" role="button" onClick={() => {CartActions.initialiseCart(restaurant._id)}}>
					        		Select restaurant
					        	</Link>
					        </p>
					      </div>
					    </div>
					  </div>
				);
		});
		// console.log('Rendering', restaurants);
		return (
			<div className="row">
			  {restaurants}
			</div>
		);
	}
});

export default RestRow;