import Reflux from 'reflux';
import OrderActions from '../actions/OrderActions';
import $ from 'jquery';

var OrderStore = Reflux.createStore({
	listenables: [OrderActions],
	init() {
		this.orders = [];
		return this.orders;
	},
	getInitialState() {
		return this.orders;
	},
	onPollOrders() {
		// console.log('Polling orders');
		$.ajax({
			url: '/api/poll',
			method: 'GET',
			success(response) {
				OrderActions.updateOrders(response);
			},
			error(err) {
				console.log('Error', err);
			}
		});
	},
	onUpdateOrders(newData) {
		if (Array.isArray(newData)) {
			this.orders = newData;
			this.trigger(this.orders);
		}
		else
			console.log(newData, 'Is not Order array!');
	},
	onHandleOrder(orderId) {
		$.ajax({
			url: '/api/handle',
			method: 'POST',
			data: {order_id: orderId},
			success(response) {
				// console.log('Handled order!');
			},
			error(err) {
				console.log(err);
			}
		});
	}
});

export default OrderStore;