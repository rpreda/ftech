import Reflux from 'reflux';
import UserActions from '../actions/UserActions';
import $ from 'jquery';
import cookies from 'cookies-js';

var UserStore = Reflux.createStore({
	listenables: [UserActions],
	init() {
		this.user = JSON.parse(cookies.get('user') || null);
		return this.user;
	},
	getInitialState() {
		return this.user;
	},
	onRegister(formData) {
		console.log(formData);
		if (formData.password != formData.confirm_password)
			return alert('Passwords don\'t match');
		$.ajax({
			url: '/api/users',
			method: 'POST',
			data: formData,
			success(response) {
				UserActions.login(formData);
			},
			error(err) {
				alert(err.responseText);
				console.log(err);
			}
		});
	},
	onLogin(formData)
	{
		console.log('Login');
		$.ajax({
			url: '/api/login',
			method: 'POST',
			data: formData,
			success(response) {
				//console.log('RESPONSE:', response);
				UserActions.update(response);
			},
			error(err) {
				alert(err.responseText);
				console.log(err);
			}
		});
	},
	onUpdate(userData) {
		console.log('Update called');
		cookies.expire('user');
		cookies.set('user', JSON.stringify(userData));
		this.user = userData;
		this.trigger(userData);
	},
	onLogout() {
		console.log('Logout');
		$.ajax({
			url: '/api/logout',
			method: 'POST',
			data: null,
			success(response) {
				UserActions.update(null);
			},
			error(err) {
				alert(err.responseText);
				console.log(err);
			}
		});
	}
});

export default UserStore;