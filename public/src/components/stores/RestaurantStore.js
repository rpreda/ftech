import Reflux from 'reflux';
import RestaurantActions from '../actions/RestaurantActions';
import $ from 'jquery';

var RestaurantStore = Reflux.createStore({
	listenables: [RestaurantActions],
	init() {
		this.restaurants = [];
		return this.restaurants;
	},
	getInitialState() {
		return this.restaurants;
	},
	onGetRestaurants() {
		var self = this;
		$.ajax({
			url: '/api/restaurants',
			method: 'GET',
			success(response) {
					// console.log('Fetched rest list', response);//Do not trigger here stuff, async shit should not be mixed with sync stuff
					RestaurantActions.updateRestaurants(response);
			},
			error(err) {
				console.log(err);
			}
		});
	},
	onUpdateRestaurants(restArray) {
		if (Array.isArray(restArray)) {
			this.restaurants = restArray;
			this.trigger(this.restaurants);
		}
		else
			console.log(restArray, ' Is not array!');
	}
});

export default RestaurantStore;