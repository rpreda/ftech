import Reflux from 'reflux';
import CartActions from '../actions/CartActions';
import $ from 'jquery';

var CartStore = Reflux.createStore({
	listenables: [CartActions],
	init(restaurant) {
		this.items = [];
		this.restaurantId = '';
		return this.items;
	},
	getInitialState() {
		return this.items;
	},
	onInitialiseCart(restaurantId)
	{
		// console.log("initialise Cart" + restaurantId);
		this.items = [];
		this.restaurantId = restaurantId;
	},
	onAddToCart(food) {
		// console.log("add to cart");
		var handled = false;
		for (var i = 0; i < this.items.length; i++) {
			if (this.items[i] === food) {
				handled = true;
				if (!this.items[i].quantity)
					this.items[i].quantity = 2;
				else
					this.items[i].quantity++;
			}
		}
		if (!handled) {
			food.quantity = 1;
			this.items.push(food);
		}
		// console.log(this.items);
		this.trigger(this.items);
	},
	onRemoveFromCart(food) {
		// console.log("remove from cart");
		for(var i = 0; i < this.items.length; i++)
		{
			if (this.items[i] === food)
			{
				this.items.splice(i, 1);
				this.trigger(this.items);
				break;
			}
		}
	},
	onOrder(specialNotes) {
		if (!specialNotes)
			specialNotes = '';
		if (this.items.length > 0 && this.restaurantId != '') {
			var orderData = {
				restaurant: this.restaurantId,
				items: [],
				notes: specialNotes
			};
			this.items.map((item, index) => {
				if (!item.quantity)
					item.quantity = 1;
				var elem = {
					item_name: item.name,
					quantity: item.quantity
				};
				orderData.items.push(elem);
				// console.log('Order data is:', orderData);
			});
			// console.log('Posting order', orderData);
			$.ajax({
				url: '/api/order',
				method: 'POST',
				contentType: "application/json; charset=utf-8",
    			data : JSON.stringify(orderData),
				success(response) {
					// console.log('Order placed!');
					CartActions.initialiseCart(this.restaurantId);
				},
				error(err) {
					console.log(err);
				}
			});
		}
		// console.log("acest buton nu face prea multe");
		// console.log('acest buton face multe acum :)');
	}
});

export default CartStore;
























