import Reflux from 'reflux';
import MenuActions from '../actions/MenuActions';
import $ from 'jquery';

var MenuStore = Reflux.createStore({
	listenables: [MenuActions],
	init() {
		this.items = [];
		return this.items;
	},
	getInitialState() {
		return this.items;
	},
	onGetMenu(restaurantId) {
		console.log('Got menu call for', restaurantId);
		if (!typeof restaurantId == "string" || !restaurantId != '')
			return console.log('Bad request param!');
		$.ajax({
			url: '/api/menu/?restaurant=' + restaurantId,
			method: 'GET',
			success(response) {
				// console.log('Fetched menu items!', response);
				MenuActions.updateMenu(response);
			},
			error(err) {
				console.log(err);
			}
		});

	},
	onUpdateMenu(newData) {
		if (Array.isArray(newData)) {
			this.items = newData;
			this.trigger(this.items);
		}
		else
			console.log(newData, 'Is not menu array!');

	}
});

export default MenuStore;