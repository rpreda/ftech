import React from 'react';
import OrderStore from './stores/OrderStore.js';
import OrderActions from './actions/OrderActions.js';
import Reflux from 'reflux';

var OrderManager = React.createClass({
	mixins: [Reflux.connect(OrderStore, 'orders')],
	componentWillMount() {
		this.timerId = setInterval(() => {
			OrderActions.pollOrders();
		}, 1000);
	},
	render() {
		var renderedOrders = [];
		this.state.orders.map((order, index) => {
			var foodItems = [];
			order.items.map((foodItem, foodIndex) => {
				foodItems.push(<li className="list-group-item" key={foodIndex}>{foodItem.item_name + ' Quantity: ' + foodItem.quantity}</li>)
			});
			renderedOrders.push(
					<div className="panel panel-info" key={index}>
					  <div className="panel-heading">Name: {order.client}</div>
					  <ul className="list-group">
					    {foodItems}
					    {order.notes != '' ? <li className="list-group-item">Details about order: {order.notes}</li> : null}
					    <li className="list-group-item" onClick = {() => {OrderActions.handleOrder(order._id)}}><div className='btn btn-success'>Processed</div></li>
					  </ul>
					</div>
				);
		});
		return (
			<div className='rest-container'>{renderedOrders}</div>
			);
	},
	componentWillUnmount() {
		clearInterval(this.timerId);
	}
});

export default OrderManager;