import Reflux from 'reflux';

var CartActions = Reflux.createActions([
	"initialiseCart",
	"addToCart",
	"removeFromCart",
	"order"
]);

export default CartActions;