import Reflux from 'reflux';

var UserActions = Reflux.createActions([
	"register",
	"login",
	"update",
	"logout"
]);

export default UserActions;
