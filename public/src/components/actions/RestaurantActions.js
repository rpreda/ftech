import Reflux from 'reflux';

var RestaurantActions = Reflux.createActions([
	"getRestaurants",
	"updateRestaurants"
]);

export default RestaurantActions;