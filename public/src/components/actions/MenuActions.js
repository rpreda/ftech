import Reflux from 'reflux';

var MenuActions = Reflux.createActions([
	"getMenu",
	"updateMenu"
]);

export default MenuActions;