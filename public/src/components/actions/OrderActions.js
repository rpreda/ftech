import Reflux from 'reflux';

var OrderActions = Reflux.createActions([
	"pollOrders",
	"updateOrders",
	"handleOrder"
]);

export default OrderActions;