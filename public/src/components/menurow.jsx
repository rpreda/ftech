import React from 'react';
import {Link} from 'react-router';
import CartActions from './actions/CartActions';
import CartStore from './stores/CartStore';

var MenuRow = React.createClass({
	render() {
		var foods = [];
		this.props.elements.map((food, index) => {
			foods.push(
					<div className = "col-sm-6 col-md-3" key = {index}>
					    <div className = "thumbnail">
					      <img src = {food.picture} alt="food thumbnail"/>
					      <div className = "caption">
					        <h3>{food.name}</h3>
					        <p>{food.ingredients}</p>
					        <p>
					        	<a className = "btn btn-primary" role = "button" onClick={() => {CartActions.addToCart(food)}} >Add to cart</a>
					        </p>
					      </div>
					    </div>
					  </div>
				);
		});
		return (
			<div className="row">
			  {foods}
			</div>
		);
	}
});

export default MenuRow;