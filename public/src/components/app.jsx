import React from 'react';
import $ from 'jquery';
import Navbar from './navbar.jsx';

var App = React.createClass({
	render() {
		return (
			<div>
				<Navbar />
				{this.props.children}
			</div>
			);
	}
});

export default App;