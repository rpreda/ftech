import React from 'react';
import $ from 'jquery';
import helpers from '../utils/helpers.js';
import UserActions from './actions/UserActions';
import UserStore from './stores/UserStore';
import { Link, Route } from 'react-router';

var Register = React.createClass({
	getInitialState() {
		return {
			signup: {
				first_name: '',
				last_name: '',
				email: '',
				password: '',
				confirm_password: '',
				phone_number: ''
			}
		}
	},
	render() {
		var self = this;

		return(

		<div className="container">
			<div className="row">
			    <div className="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3">
					<form role="form">
						<h2 className="center" >Please Sign Up </h2>
						<hr className="colorgraph"/>
						<div className="row">
							<div className="col-xs-12 col-sm-6 col-md-6">
								<div className="form-group">
			                        <input
			                        	type="text"
			                        	className="form-control input-lg"
			                        	placeholder="First Name"
			                        	value = {self.state.signup.first_name}
			                        	onChange={(e) => {
                                        helpers.valueChanged.call(self, 'signup.first_name', e.target.value);
                                    }}
			                        />
								</div>
							</div>
							<div className="col-xs-12 col-sm-6 col-md-6">
								<div className="form-group">
									<input 
										type="text"
										className="form-control input-lg"
										placeholder="Last Name"
										tabIndex="2"
										value = {self.state.signup.last_name}
										onChange={(e) => {
                                        helpers.valueChanged.call(self, 'signup.last_name', e.target.value);
                                    }}
									/>
								</div>
							</div>
						</div>
						<div className="form-group">
							<input
								type="text"
								className="form-control input-lg"
								placeholder="Phone Number"
								tabIndex="3"
								value = {self.state.signup.phone_number}
								onChange={(e) => {
                                        helpers.valueChanged.call(self, 'signup.phone_number', e.target.value);
                                    }}
							/>
						</div>
						<div className="form-group">
							<input
								type="email" 
								className="form-control input-lg" 
								placeholder="Email Address" 
								tabIndex="4"
								value = {self.state.signup.email}
								onChange={(e) => {
                                        helpers.valueChanged.call(self, 'signup.email', e.target.value);
                                    }}
							/>
						</div>
						<div className="row">
							<div className="col-xs-12 col-sm-6 col-md-6">
								<div className="form-group">
									<input type="password" 
										className="form-control input-lg" 
										placeholder="Password" 
										tabIndex="5"
										value = {self.state.signup.password}
										onChange={(e) => {
                                        helpers.valueChanged.call(self, 'signup.password', e.target.value);
                                    }}
									/>
								</div>
							</div>
							<div className="col-xs-12 col-sm-6 col-md-6">
								<div className="form-group">
									<input 
										type="password"
										className="form-control input-lg"
										placeholder="Confirm Password"
										tabIndex="6"
										value = {self.state.signup.confirm_password}
										onChange={(e) => {
                                        helpers.valueChanged.call(self, 'signup.confirm_password', e.target.value);
                                    }}
									/>
								</div>
							</div>
						</div>			
						<hr className="colorgraph"/>
						<div className="row">
							<div className="col-xs-12 col-md-6 center max-width" onClick={() => {UserActions.register(self.state.signup)}}>
								<Link to = '/' className="btn btn-primary btn-block btn-lg">
									Register
								</Link>
							</div>
						</div>
					</form>
			</div>

			</div>
		</div> 
		);
	}
});

export default Register;