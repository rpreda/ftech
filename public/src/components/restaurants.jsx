import React from 'react';
import Row from './restrow.jsx';
import RestaurantActions from './actions/RestaurantActions.js';
import RestaurantStore from './stores/RestaurantStore.js';
import Reflux from 'reflux';

var Restaurants = React.createClass({
	mixins: [Reflux.connect(RestaurantStore, 'restaurants')],
	componentWillMount() {
		RestaurantActions.getRestaurants();
	},
	render() {
		var restRows = [];
		const chunk = 4;
		for (var i = 0, j = this.state.restaurants.length; i < j; i += chunk) {
		    restRows.push(<Row elements = {this.state.restaurants.slice(i , i + chunk) } key = {i}/>);
		}
		return (
				<div className='container rest-container'>
					{restRows}
				</div>
			);
	}
});

export default Restaurants;