import React from 'react';
import UserStore from './stores/UserStore';
import Reflux from 'reflux';
import Restaurants from './restaurants.jsx';
import OrderManager from './ordermanager.jsx'

var Mainpage = React.createClass({
	mixins: [Reflux.connect(UserStore, 'user')],
	render() {
		return (
				<div>
					{this.state.user && this.state.user.owner ? <OrderManager/> : <Restaurants/>}
				</div>
			);
	}
});

export default Mainpage;