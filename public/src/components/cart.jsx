import React from 'react';
import Reflux from 'reflux';
import CartActions from './actions/CartActions';
import CartStore from './stores/CartStore';
import Row from './cartrow.jsx';
import { Link, Route } from 'react-router';
import helpers from '../utils/helpers.js';


var Cart = React.createClass({
	mixins: [Reflux.connect(CartStore, 'cart')],
	getInitialState() {
		return {
			notes: {
				data: ''
			}
		}
	},
	render() {
		// console.log(this.state.cart);
		var cartRows = [];
		const chunk = 4;
		for (var i = 0, j = this.state.cart.length; i < j; i += chunk) {
		    cartRows.push(<Row elements = {this.state.cart.slice(i , i + chunk) } key = {i}/>);
		}
		var self = this;
		return (
			<div className='container rest-container'>
				{cartRows.length > 0 ?
				<div>
					<input
						type="text"
						className="form-control input-lg"
						placeholder="Please enter special notes here"
						tabIndex="4"
						value = {self.state.notes.data}
						onChange={(e) => {
	                         helpers.valueChanged.call(self, 'notes.data', e.target.value);
	                         }}
					/>
					<div className="row" onClick={() => {CartActions.order}}>
								<div className="col-xs-12 col-md-6 center max-width" onClick={() => {CartActions.order(this.state.notes.data)}}>
									<Link to='/' className="btn btn-default btn-block btn-lg">
										Submit order
									</Link>
								</div>
					</div>
				</div>
				: 
					<div className="alert alert-warning" role="alert">The cart is currently empty!</div> 
				}
				<div className="margin-top">
					{cartRows}
				</div>
			</div>
			);
	}
});

export default Cart;