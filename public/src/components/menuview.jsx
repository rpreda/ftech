import React from 'react';
import Row from './menurow.jsx';
import Reflux from 'reflux';
import MenuStore from './stores/MenuStore.js';
import MenuActions from './actions/MenuActions.js';
import CartActions from './actions/CartActions.js';

var menuview = React.createClass({
	mixins: [Reflux.connect(MenuStore, 'menu')],
	componentWillMount() {
		MenuActions.getMenu(this.props.params.restaurant);
	},
	render() {
		// console.log(this.state.menu);
		var menuRows = [];
		const chunk = 4;
		for (var i = 0, j = this.state.menu.length; i < j; i += chunk) {
		    menuRows.push(<Row elements = {this.state.menu.slice(i , i + chunk) } key = {i}/>);
		}
		return (
			<div className='rest-container container'>
				{menuRows}
			</div>
			);
	}
});

export default menuview;