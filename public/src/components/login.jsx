import React, { Component } from 'react';
import $ from 'jquery';
import helpers from '../utils/helpers.js';
import UserActions from './actions/UserActions';
import UserStore from './stores/UserStore';
import { Link, Route } from 'react-router';

var Login = React.createClass({
	getInitialState() {
		return {
			login: {
				email: '',
				password: ''
			}
		}
	},
	render() {
		var self = this;

		return(

		<div className="container">
			<div className="row">
			    <div className="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3">
					<form role="form">
						<h2 className="center" > Login </h2>
						<hr className="colorgraph"/>
						<div className="form-group">
							<input
								type="email"
								className="form-control input-lg"
								placeholder="Email Address"
								tabIndex="4"
								value = {self.state.login.email}
								onChange={(e) => {
                                        helpers.valueChanged.call(self, 'login.email', e.target.value);
                                    }}
							/>
						</div>
						<div className="form-group">
							<input type="password"
										className="form-control input-lg"
										placeholder="Password"
										tabIndex="5"
										value = {self.state.login.password}
										onChange={(e) => {
                                        helpers.valueChanged.call(self, 'login.password', e.target.value);
                                    }}
									/>
						</div>	
						<hr className="colorgraph"/>
						<div className="row">
							<div className="col-xs-12 col-md-6 center max-width" onClick={() => {UserActions.login(self.state.login)}}>
								<Link to='/' className="btn btn-primary btn-block btn-lg">
									Login
								</Link>
							</div>
						</div>
					</form>
			</div>

			</div>
		</div> 
		);
	}
});

export default Login;