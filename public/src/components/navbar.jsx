import React from 'react';
import $ from 'jquery';
import { Link, Route } from 'react-router';
import Reflux from 'reflux';
import UserActions from './actions/UserActions';
import UserStore from './stores/UserStore';

var Navbar = React.createClass({
	mixins: [Reflux.connect(UserStore, 'user')],
	render() {
		var self = this;
		return (
			<nav className = 'navbar navbar-default navbar-position'>
				<div className = 'container-fluid'>
					<ul className='navbar-right navbar-nav nav'>
						<li><Link to = '/'>{this.state.user ? this.state.user.first_name : 'Welcome!'}</Link></li>
						{this.state.user ? <li><Link to = '/cart'> Cart </Link></li> : null }
					</ul>
						<ul className='navbar-right navbar-nav nav' role='group' aria-label='...'>
							{
								this.state.user ?
								<li><a></a></li> :
								<li><Link to="/login">Login</Link></li>
							}
							{
								this.state.user ?
								<li><Link to = '/' onClick = {UserActions.logout}>Logout</Link></li> :
								<li><Link to = "/register">Sign up</Link></li>
							}
						</ul>
				</div>
			</nav>
			);
	}
});

export default Navbar;