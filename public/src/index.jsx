import ReactDOM from 'react-dom';
import React from 'react';
import { Router, Route, IndexRoute, Link } from 'react-router';
import { browserHistory } from 'react-router';
import Navbar from './components/navbar.jsx';
import Register from './components/register.jsx';
import Login from './components/login.jsx';
import App from './components/app.jsx';
import Mainpage from './components/mainpage.jsx'; 
import MenuView from './components/menuview.jsx';
import Cart from './components/cart.jsx';

ReactDOM.render(
	<Router history={browserHistory}>
		<Route path='/' component={App}>
			<Route path='register' component={Register}/>
			<Route path='login' component={Login}/>
			<Route path='/menu/:restaurant' component={MenuView}/>
			<Route path='cart' component={Cart}/>
			<IndexRoute component={Mainpage}/>
		</Route>
	</Router>, document.getElementById('app'));