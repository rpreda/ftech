var TOKEN_SIZE = 64;
var DEFAULT_TTL = 604800; //1 week

var client = require('redis').createClient();
var crypto = require('crypto');
var redisClient = require('redis').createClient();

redisClient.on('error', function (err) {
    console.error(err);
});

client.on('error', (err) => {
	console.log(err);
});

exports.newToken = function(callback) {
	crypto.randomBytes(TOKEN_SIZE, (err, buff) => {
		if (err) {
			console.log('Error generating random token');
			callback(null);
		}
		var token = buff.toString('hex');
		callback(token);
	});
};

exports.registerUser = function(user, token, ttl, callback) {
	if (!user || !token || token == '')
		return callback(null);
	ttl = ttl || DEFAULT_TTL;
	if (typeof ttl != 'number')
		return callback(null);
	redisClient.set(token, JSON.stringify(user), (err, reply) => {
		if (err) {
			console.log(err);
			return callback(false);
		}
		if (reply)
			redisClient.expire(token, ttl, (err, reply) => {
				if (err) {
					console.log(err);
					return callback(false);
				}
				if (reply) {
					callback(true, token);
				}
			});
	});
};

exports.getUserData = function(token, callback) {
	if (!token)
		callback();
	redisClient.get(token, (err, user) => {
		if (err) {
			console.log(err);
			return callback();
		}
		if (user)
			return callback(user);
		return callback();
	});
};

exports.expireUser = function(token) {
	redisClient.expire(token, 0, (err, reply) => {
		if (err) {
			console.log(err);
			console.log('Failed to expire user redis data');
		}
	});
};