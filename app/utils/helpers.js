var crypto = require('crypto');
var params = require('../params.js');

exports.hash = function (string) {
    return crypto.createHmac('sha256', params.secret)
        .update(string)
        .digest('hex');
};
