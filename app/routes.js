var express = require('express');
var UserController = require('./controllers/user.js');
var DataController = require('./controllers/data.js');
var OrderController = require('./controllers/order.js');
var cookie = require('cookie');
var router = express.Router();
var Token = require('./utils/tokengen.js');

router.use((req, res, next) => {
	if (req.headers.cookie) {//Here also load up the user from redis and save it into the request
		var cookies = cookie.parse(req.headers.cookie);
		req.auth_token = cookies.auth_token;
		if (req.auth_token) {
			Token.getUserData(req.auth_token, (user) => {
				if (!user)
				{
					// console.log('User not found for token: ' + req.auth_token);
				}
				else {
					//console.log('User retrieved from redis.');//Remove me
					req.userData = JSON.parse(user);
				}
				next();
			});
		}
		else
			next();
	}
	else
		next();
});

/** ROUTES **/

	/* User api routes */
	router.post('/users', UserController.create);
	router.post('/login', UserController.login);
	router.post('/logout', UserController.logout);

	/* Data api routes */
	router.get('/restaurants', DataController.getRestaurants);
	router.get('/menu', DataController.getMenu);//Get parameter restaurant should be set with the id of the restaurant

	/* Order api routes */
	router.get('/order', OrderController.getOrderHistory);//Get the users order history, only works if user is logged in
	router.post('/order', OrderController.postOrder);
	router.post('/handle', OrderController.handleOrder);
	router.get('/poll', OrderController.getOrders);


	//Dev stuff *REMOVE*
	router.get('/test', UserController.devTestCall);

exports.router = router;