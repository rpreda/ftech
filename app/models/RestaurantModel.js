var mongoose = require('mongoose');
var ObjectId = mongoose.Schema.ObjectId;

var RestaurantSchema = new mongoose.Schema({
	name: {
		type: String,
		required: true
	},
	image: {
		type: String,
		required: false
	},
	desc: {
		type: String,
		required: true
	},
	full: {
		type: Boolean,
		required: true,
		default: false
	},
	owner: {
		type: ObjectId,
		ref: 'User'
	},
	menu: [{
		name: String,
		picture: String,
		ingredients: String,
		category: Number,
		price: Number
	}]
});

module.exports = mongoose.model('Restaurant', RestaurantSchema);