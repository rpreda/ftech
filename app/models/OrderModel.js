var mongoose = require('mongoose');
var ObjectId = mongoose.Schema.ObjectId;

var OrderSchema = new mongoose.Schema({
	restaurant: {
		type: ObjectId,
		required: true
	},
	client: {
		type: String,
		required: true
	},
	items: {
		type: [{
			item_name: String,
			quantity: Number
		}],
		required: true
	},
	notes: {
		type: String,
		required: false,
		default: 'No special user notes.'
	},
	handled: {
		type: Boolean,
		required: true,
		default: false
	}
});

module.exports = mongoose.model('Order', OrderSchema);