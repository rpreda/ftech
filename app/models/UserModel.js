var mongoose = require('mongoose');
var ObjectId = mongoose.Schema.ObjectId;

var UserSchema = new mongoose.Schema({
	email: {
		type: String,
		unique: true,
		required: true
	},
	password: {
		type: String,
		required: true,
		select: false
	},
	first_name: {
		type: String,
		required: true
	},
	last_name: {
		type: String,
		required: true
	},
	active: {
		type: Boolean,
		default: true,
		required: true
	},
	phone: {
		type: String,
		required: false
	},
	owner: {
		type: Boolean,
		default: false,
		required: true
	},
	owned: {
		type: ObjectId,
		ref: 'Restaurant'
	}
});

module.exports = mongoose.model('User', UserSchema);
