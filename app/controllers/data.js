var RestModel = require('../models/RestaurantModel.js');

exports.getRestaurants = (req, res) => {
	var crudObj = {
		q: {},
		o: {menu: 0}
	}
	RestModel.find(crudObj.q, crudObj.o, (err, restaurants) => {
		if (err)
			return res.status(422).send(err);
		return res.status(200).send(restaurants);
	});
};

exports.getMenu = (req, res) => {
	if (!req.query || !req.query.restaurant)
		return res.status(400).send('Parameter missing');
	var crudObj = {
		q: {
			_id: req.query.restaurant
		}
	}
	RestModel.findOne(crudObj.q, (err, rest) => {
		if (err)
			return res.status(404).send(err);
		if (rest)
			return res.status(200).send(rest.menu);
	});

};