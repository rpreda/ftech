var OrderModel = require('../models/OrderModel.js');

exports.getOrderHistory = (req, res) => {
	if (!req.userData)
		return res.status(401).send('User not logged in!');
	var crudObj = {
		q: {
			client: new ObjectID(req.userData._id),
			handled: true
		}
	}
	OrderModel.find(crudObj.q, (err, orders) => {
		if (err)
			return res.status(500).send(err);
		if (orders)
			return res.status(200).send(orders);
	});
}

exports.postOrder = (req, res) => {
	if (!req.userData)
		return res.status(401).send('User not logged in!');
	//should get the restaurant ID and the array of products from the request the notes are optional
	var order = req.body;//here should be a check to see if the entered restaurant and product ID's are valid
	// console.log(req.body);
	if (!order.restaurant)
		return res.status(400).send('No restaurant ID!');
	if (!order.items || !Array.isArray(order.items)) {
		// console.log(order);
		return res.status(400).send('Missing or invalid items array!');
	}
	order.client = req.userData.first_name + ' ' + req.userData.last_name;
	var newOrder = new OrderModel(order);
	if (newOrder.save()) {
		return res.status(200).send('Order placed!');
	}
	else
		return res.status(500).send('Error creating order!');
};

exports.handleOrder = (req, res) => {
	if (!req.userData || !req.userData.owner)
		return res.status(401).send('User not logged in or user does not have permision to handle order!');
	var crudObj = {
		q: {
			_id: req.body.order_id,
			restaurant: req.userData.owned
		}
	};
	OrderModel.findOne(crudObj.q, (err, order) => {
		if (err)
			return res.status(500).send('Order could not be retrieved!');
		if (order) {
			order.handled = true;
			order.save();//Here a confirmation email could be sent to the user
			return res.status(200).send('Order handled!');
		}
	});
};

exports.getOrders = (req, res) => {
	if (!req.userData || !req.userData.owner)
		return res.status(401).send('User not logged in or user does not have permision to handle order!');
	var crudObj = {
		q: {
			restaurant: req.userData.owned,
			handled: false
		}
	};
	OrderModel.find(crudObj.q, (err, orders) => {
		if (err)
			return res.status(404).send('No new orders found!');
		if (orders)
			return res.status(200).send(orders);//Populate each order from the backend and not from the frontend, if easier
	});
};