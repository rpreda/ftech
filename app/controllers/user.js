var Validations = require('../utils/validations.js');
var UserModel = require('../models/UserModel.js');
var Helpers = require('../utils/helpers.js');
var Token = require('../utils/tokengen.js');
var cookie = require('cookie');


//Dev depends
var RestModel = require('../models/RestaurantModel.js');
var OrderModel = require('../models/OrderModel.js');
var MockData = require('./mockdata.js');

/* MOCK-DATA */



exports.devTestCall = (req, res) => {
	//Mock data builder
	// MockData.restaurants.map((data, key) => {
	// 	data.menu = MockData.menu;
	// 	new RestModel(data).save();
	// });
	UserModel.findOne({email: "rares.preda98@gmail.com"}, (err, user) => {
		user.owner = true;
		user.owned = '57d0817839c93fa722ff82b3';
		user.save();
	});

	/* Stuff for testing the relations between collecitons
	// new RestModel({"name":"Jazzy","image":"http://dummyimage.com/500x500.jpg/5fa2dd/ffffff","desc":"proin risus praesent lectus vestibulum quam sapien varius ut blandit non interdum in ante vestibulum ante ipsum primis in faucibus orci luctus et"}).save();
	// new RestModel({"name":"Pixoboo","image":"http://dummyimage.com/500x500.jpg/cc0000/ffffff","desc":"fusce congue diam id ornare imperdiet sapien urna pretium nisl ut volutpat sapien arcu sed augue aliquam erat volutpat in congue etiam justo etiam pretium iaculis"}).save();
	UserModel.findOne({email: 'rares.preda98@gmail.com'}).populate('owned').exec((err, user) => {
		if (!err) {
			console.log(user);
			// RestModel.findOne({name: 'Jazzy'}, (err, rest) => {
			// 	console.log(rest, user);
			// 	RestModel.find({_id: user.owned}, (err, rest) => {})
			// 	// user.owner = true;
			// 	// user.owned = rest;
			// 	// user.save();
			// });
		}
	});
	*/
	//console.log(req.query); get parameters end up here
	// RestModel.findOne({name: 'Jazzy'}, (err, rest) => { Population of the menu for a restaurant
	// 	if (!err) {
	// 		rest.menu = [{name: 'Item 1', picture: 'pic', ingredients: 'contains', category: 2, price: 9001}, 
	// 					 {name: 'Item 2', picture: 'pic', ingredients: 'contains', category: 2, price: 9001}];
	// 		rest.save();
	// 	}
	// });
	res.status(200).send('Call ok!');
};

exports.create = function(req, res) {
	user = req.body;
	if (!user.first_name || !user.last_name)
		return res.status(422).send('Missing name').send();
	if (!user.password)
		return res.status(422).send('Missing password').send();
	if (!Validations.validatePass(user.password))
		return res.status(422).send('Invalid password').send();
	user.password = Helpers.hash(user.password);
	if (!user.email)
		return res.status(422).send('Missing email').send();
	if (!Validations.validateEmail(user.email))
		return res.status(422).send('Invalid email').send();
	var newUser = new UserModel(user);
	newUser.save((err) => {
		if (err) {
			//console.log('Error is', err);
			return res.status(500).send('Db error');
		}
		//console.log('Register OK');
		return res.status(200).send('{"success": "true"}');
	});
};

exports.login = function(req, res) {
	if (req.userData) {
		return res.status(400).send('User already registered!');
	}
	inputData = req.body;
	if (!(inputData && inputData.email && inputData.password)) {
		req.status(400).send("Invalid login data!");
	}
	var crudObj = {
		q: {
			password: Helpers.hash(inputData.password),
			email: inputData.email
		}
	};
	UserModel.findOne(crudObj.q, (err, user) => {
		if (err)
			return console.log(err);
		Token.newToken((token) => {
			if (token) {
				Token.registerUser(user, token, 604800, (result) => {
					if (result) {
						res.writeHead(200, {
							'Set-Cookie': 'auth_token=' + token +'; Path=/;',
							'Content-Type': 'application/json',
						});
						return res.end(JSON.stringify(user));
					}
					else
						return res.status(500).send('Failed to authenticate user');
				});
			}
			else
				return res.status(500).send('Failed to authenticate user');
		});
	});
};

exports.logout = function(req, res) {
	if (req.userData) {
		if (req.auth_token) {
			Token.expireUser(req.auth_token);
			return res.status(200).send();
		}
	}
	res.status(500).send('User was not logged in!');
}