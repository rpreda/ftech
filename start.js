var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var Param = require('./app/params.js');
var mongoose = require('mongoose');
var bodyParser = require('body-parser');
var Routes = require('./app/routes.js');
var TokenGenerator = require('./app/utils/tokengen.js')

var app = express();

mongoose.connect(Param.db);
/** Middleware **/
app.use(cookieParser());
//->bodyparser
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

//->api routes installed
app.use('/api/', Routes.router);

app.use(express.static(path.join(__dirname, 'public')));
/** ========== **/

app.all('/*', (req, res, next) => {
	//res.status(404);
	//res.sendFile(__dirname + '/public/four-oh-four.html');
	res.redirect('/');
});

app.listen(80, () => {
	console.log('App listening on port 80!');
});